package me.aecsocket.locale;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

/** The main plugin class. */
public class Locale extends JavaPlugin {
    private static Locale instance;

    @Override
    public void onEnable() {
        instance = this;
        saveDefaultConfig();
    }

    /** Gets the specified String in a specific locale as defined by the config.
     * @param locale The locale as defined in the config.
     * @param label The label as defined in the config.
     * @return The string from the config.
     */
    public static String getLabel(String locale, String label) {
        FileConfiguration config = instance.getConfig();
        String localePath = String.format("locale.%s", label);
        if (!config.contains(localePath))
            localePath = String.format("locale.%s", config.getString("locale.fallback-locale"));
        String path = String.format("%s.%s", localePath, label);
        return (config.contains(path) ? config.getString(path) : config.getString("locale.fallback-label"));
    }

    /** Gets the fallback locale as defined by the config.
     * @return The fallback locale.
     */
    public static String getFallbackLocale() {
        return instance.getConfig().getString("locale.fallback-locale");
    }

    /** Gets the fallback text as defined by the config.
     * @return The fallback text.
     */
    public static String getFallbackText() {
        return instance.getConfig().getString("locale.fallback-text");
    }
}
