# Locale
A simple locale tool for Spigot.
This is one of the simplest - if not **the** simplest - locale tools for a Minecraft server on Spigot.
## For server owners
Edit the `config.yml` file in `$SERVER_ROOT/plugins/Locale` to add labels. The format is simple:
```yml
locale:
  fallback-locale: "en_us" # If the locale that is requested is not found, this is used instead
  fallback-label: "Text not found" # If the locale used does not contain the label requested
  en_us: # The locale
    shovel: "Shovel" # The label, with a value of the translation
  en_gb:
    shovel: "Spade"
```
## For developers
There are three methods available. Using this, you can get a String specified by the config, the fallback locale and the fallback text.
```
me.aecsocket.locale.Locale.getLabel(String locale, String label)
  Gets the specified String in a specific locale as defined by the config.
    locale: The locale as defined in the config.
    label: The label as defined in the config.
  returns: The string from the config.
```
(Note: `Player#getLocale` returns a String formatted as `language_dialect`. It is recommended to specify the locales as used in `#getLocale`. You can find a full list [here](https://minecraft.gamepedia.com/Language#Available_languages).)


```
me.aecsocket.locale.Locale.getFallbackLocale()
  Gets the fallback locale as defined by the config.
  returns: The fallback locale.
```


```
me.aecsocket.locale.Locale.getFallbackText()
  Gets the fallback text as defined by the config.
  returns: The fallback text.
```
## Download
### Compilation
Download the repo/clone it, add the Spigot API JAR file as a dependency and build (e.g. Build Artifact in IntelliJ).
### Download
Currently, there are no prebuilt JARs for download.
